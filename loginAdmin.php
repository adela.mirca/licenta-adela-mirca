<!DOCTYPE html>
<html>
<head>
    <title>Cont Admin</title>
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">

    <?php
        include_once('header.php');
        echo"<br>";
        include_once('nav.php');
    ?>
</head>
<body>
    <div id='content'>
        <form id="form" action="logAdmin.php" method="post">
            <h2>Cont Administrator</h2>
            <?php if (isset($_GET['error'])){ ?>
                <p class="error"><?php echo $_GET['error']; ?></p>
            <?php } ?>
            <label>Email</label>
            <input type="text" name="email" placeholder="Email"><br>

            <label>Parola</label>
            <input type="password" name="password" placeholder="Password"><br>

            <button type="submit">Login</button>
        </form>
    </div>
    <?php
        include_once('footer.php');
    ?>
</body>
</html>