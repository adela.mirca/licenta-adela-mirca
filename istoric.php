<!DOCTYPE html>
<html>
    <head>
        <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">  
        <style>
            .cancelSbutton {
                position: absolute;
                top: 150px;
                right: 30px;
            }
            #content{
                height: 82vh !important;
            }
        </style>
       <?php
            include_once('header.php');
            echo"<br>";
            ?>
    </head>
    <body>
        <div id="content">
        <?php
            if(isset($_GET["id_pac"])){
                $id_pac = $_GET["id_pac"];

                include "db_conn.php";

                $sql = "SELECT * FROM pacienti WHERE id_pac=$id_pac";
                $result = mysqli_query($conn, $sql);
                while($row = $result->fetch_assoc()){
                    if($row['istoric_fam']!=NULL){
                        echo"<div id='viewform'>
                        <h4>Istoricul de Familie a pacientului $id_pac: <br><br> $row[istoric_fam] </h4></div>";
                             
                    }
                    else{
                        echo"<div id='viewform'>
                        <h4>Pacientul $id_pac nu are istoric genetic relevant.<h4>
                        </div>";
                    }    
                }   
                echo "<a class='cancelSbutton' href='vizualizare.php?id_pac=$id_pac' role='button'>Inapoi</button></a> ";             
            }
            
        ?>
        </div>
        
        <?php
        include_once('footer.php');
        ?>
    </body>   

</html>