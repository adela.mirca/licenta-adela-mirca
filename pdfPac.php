

<?php                
include "db_conn.php";
include_once('tcpdf/tcpdf.php');

$id_pac=$_GET['id_pac'];

$query = "SELECT id_pac, nume, prenume, CNP, dataN, oras, mama, tata, diagnostic, istoric_fam, id_doc
FROM pacienti WHERE id_pac='$id_pac' ";             
$result = mysqli_query($conn,$query);   
$count = mysqli_num_rows($result);  
if($count>0) 
{
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

	//----- Code for generate pdf
	$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);  
	//$pdf->SetTitle("Export HTML Table data to PDF using TCPDF in PHP");  
	$pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
	$pdf->SetDefaultMonospacedFont('times');  
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
	$pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
	$pdf->setPrintHeader(false);  
	$pdf->setPrintFooter(false);  
	$pdf->SetAutoPageBreak(false);  
	$pdf->SetFont('times', '', 12);  
	$pdf->AddPage(); //default A4
	//$pdf->AddPage('P','A5'); //when you require custome page size 

    //$pdf->Image('logo.jpg', $x = 15, $y = 15, $w = 10, $h = 10, $type = '', $link = '', $align = 'left', $resize = false, $dpi = 100, $palign = '', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false);
	
	$content = ''; 

	$content .= '
	<style type="text/css">
	body{
	font-size:12px;
	line-height:24px;
	font-family: Cambria, Cochin, Georgia, Times, "Times New Roman", serif;
	background-color:#8ad7d1;
	}
	</style>    
	<table cellpadding="0" cellspacing="0" style="background-color:#d3d3d3; border:3px solid #8ad7d1;width:100%;height:100%;">
	<table style="width:100%;height:100%;" >
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr><td colspan="2" align="center"><b>MENTAL HEALTH CLINIC</b></td></tr>
    <br>
    <tr><td colspan="2" align="center" style="font-size=5px;"><b>FISA PACIENT</b></td></tr>
	<tr><td colspan="2" align="center"><b>ID: </b>'.$row['id_pac'].' </td></tr>
    <tr><td colspan="2" align="center"><b>Pacient:  </b>'.$row['nume'].' '.$row['prenume'].'</td></tr>
    <tr><td colspan="2" align="center"><b>CNP: </b> '.$row['CNP'].'</td></tr>
    <tr><td colspan="2" align="center"><b>Data Nasterii:  </b>'.$row['dataN'].'</td></tr>
    <tr><td colspan="2" align="center"><b>Oras:  </b>'.$row['oras'].'</td></tr>
    <tr><td colspan="2" align="center"><b>Nume mama:  </b>'.$row['mama'].' </td></tr>
    <tr><td colspan="2" align="center"><b>Nume tata:  </b>'.$row['tata'].' </td></tr>
    <tr><td colspan="2" align="center"><b>Istoric Familiar:  </b>'.$row['istoric_fam'].' </td></tr>
    <tr><td colspan="2" align="center"><b>ID doc:  </b>'.$row['id_doc'].' </td></tr>

	<tr class="heading" style="background:#eee;border-bottom:1px solid #ddd;font-weight:bold;">
		<td>
			TYPE OF WORK
		</td>
		<td align="right">
			AMOUNT
		</td>
	</tr>
		
	<tr><td colspan="2">&nbsp;</td></tr>
	</table>
</table>'; 
$pdf->writeHTML($content);

$file_location = "detaliiPac.php?id_pac=$id_pac"; //add your full path of your server
//$file_location = "/opt/lampp/htdocs/examples/generate_pdf/uploads/"; //for local xampp server

$datetime=date('dmY_hms');
$file_name = "INV_".$datetime.".pdf";
ob_end_clean();

$pdf->Output($file_name, 'I'); // I means Inline view
/*if($_GET['ACTION']=='VIEW') 
{
	$pdf->Output($file_name, 'I'); // I means Inline view
} 
else if($_GET['ACTION']=='DOWNLOAD')
{
	$pdf->Output($file_name, 'D'); // D means download
}
else if($_GET['ACTION']=='UPLOAD')
{
$pdf->Output($file_location.$file_name, 'F'); // F means upload PDF file on some folder
echo "Upload successfully!!";
}*/

//----- End Code for generate pdf
	
}
else
{
	echo 'Record not found for PDF.';
}

?>