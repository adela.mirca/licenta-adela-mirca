<!DOCTYPE html>
<html>
    <head>
        <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">  
        <?php
        include_once('header.php');
        ?>
        <style>
        #vizualizare{
        height: 82vh;
        }
        </style>
    </head>
    <body>
        <div id="vizualizare">
            <div id="viewform">
            <?php
                if(isset($_GET["id_pac"])){
                    $id_pac = $_GET["id_pac"];

                    include "db_conn.php";

                    $sql = "SELECT * FROM pacienti WHERE id_pac=$id_pac";
                    $result = mysqli_query($conn, $sql);
                    while($row = $result->fetch_assoc()){
                        echo "<h2>Pacientul: $row[nume] $row[prenume]</h2>";
                        echo "<h3>Diagnostic: $row[diagnostic] </h3>";
                        echo "<br>";

                    echo "<a class='viewbutton' href='detaliiPac.php?id_pac=$row[id_pac]'>Detalii</a>";
                    echo"<br><br>";
                    echo "<a class='viewbutton' href='programari.php?id_pac=$row[id_pac]'>Programari</a>";
                    echo"<br><br>";
                    echo "<a class='viewbutton' href='simptome.php?id_pac=$row[id_pac]'>Simptome</a>";
                    echo"<br><br>";
                    echo "<a class='viewbutton' href='modificare.php?id_pac=$row[id_pac]'>Modificare Diagnostic</a>";
                    echo"<br><br>";
                    echo "<a class='viewbutton' href='reminder.php?id_pac=$row[id_pac]'>Setare Reminder</a>";
                    echo"<br><br>";
                    echo "<a class='viewbutton' href='istoric.php?id_pac=$row[id_pac]'>Istoric Genetic</a>";
                    echo"<br><br>";
                    echo "<a class='inapoibutton' href='listaPacienti.php'>Inapoi</a>";
                    }
                   
                    
                    
                }
            ?>
            </div>
            </div>
            <?php
            include_once('footer.php');
            ?>
    </body>

</html>

