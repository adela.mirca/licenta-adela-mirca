<?php 
include_once('header.php');
include_once('nav.php');
?>
<style>
    #content2 {
        position: relative;
        background-image: url('mental1.jpg');
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        padding: 20px;
        color: #000000;
    }

    #content2 h1,
    #content2 h2,
    #content2 h3 {
        position: relative;
        z-index: 1;
    }
</style>
<div id='content2'>
    <h1>Clinica de Sanatate Mintala</h1>
    <h2>Invatati sa aveti grija de mintea dvs!</h2>
    <h3>
        Am creat acest site pentru a inspira siguranta tuturor persoanelor care trec printr-o perioada grea si nu stiu ce sa faca sau la cine sa apeleze.
        <br>Scopul nostru este ca acest subiect sa nu mai fie unul neglijat si oricine simte ca nu este bine pe plan psihic sa ne poata contacta cu usurinta.
        <br>Va suntem la dispozitie pentru orice fel de intrebare si pentru a gasi un specialist care se adapteaza la cerintele dvs.</br>
        <br>Va simtiti tristi, fara energie sau fara vointa? Sau ati auzit ca reactiile voastre sunt prea exagerate la anumite intamplari?
        <br>Nimeni nu ar trebui sa treaca prin aceste momente singur. Iar daca nu doriti sa comunicati cu persoanele cunoscute noi suntem aici incercand sa va facem sa va simtiti mai bine fara sa va judecam.
    </h3>
</div>
<?php
include_once('footer.php');
?>
