<!DOCTYPE html>
<html>
<head>
    <title>LOGIN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <style>
        .contact-info {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 10px;
        }

        .contact-info i {
            margin-right: 10px;
            color: #555555;
        }

        a {
            color: #007bff;
            text-decoration: none;
        }
    </style>

    <?php
        include_once('header.php');
        include_once('nav.php');
    ?>
   
</head>
<body>
    <div id="contentcontact">
        <div id="contactform">
            <h1>CONTACT</h1>
            <div class="contact-info">
                <i class="fas fa-envelope"></i>
                <h2> clinica@gmail.com</h2>
            </div>
            <div class="contact-info">
                <i class="fas fa-phone"></i>
                <h2>+40 730 121 936</h2>
            </div>
            <div class="pin-icon">
            <h2><i class="fas fa-map-marker-alt"></i> Strada Sanatatii, Timisoara</h2>
            </div>
        </div>
    </div>
    <?php
        include_once('footer.php');
    ?>
</body>
</html>