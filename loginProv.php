<?php
include "db_conn.php";

if(isset($_POST['email']) && isset($_POST['password']))
{
    function validate($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    $email = validate($_POST['email']);
    $password = validate($_POST['password']);

    if(empty($email)){
        header("Location: login.php?error=Introduceti o adresa email");
        exit();
    }
    else if(empty($password)){
        header("Location: login.php?error=Introduceti parola");
        exit();
    }
    else{
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            header("Location: login.php?error=Introduceti o adresa de mail valida");
        exit(); 
          }
        else{
        $sql = "SELECT * FROM logareDoc WHERE email='$email'";

        $result = mysqli_query($conn, $sql);

        

        if(mysqli_num_rows($result) === 1){
            $row = mysqli_fetch_assoc($result);
            if($row['email'] === $email && password_verify($password, $row['parola'])){
                header("Location: listaPacienti.php");
            }
        }
        else{
                $sql1 = "SELECT * FROM logarePac WHERE email='$email'";

                $result1 = mysqli_query($conn, $sql1);

                if(mysqli_num_rows($result1) === 1){
                    $row1 = mysqli_fetch_assoc($result1);
                    if($row1['email'] === $email && password_verify($password, $row1['parola'])){
                        $CNP = $row1['CNP'];
                        header("Location: MainPacient.php?CNP=$row1[CNP]");
                    }
                }
                else{
                        header("Location: login.php?error=Email sau parola incorecte");
                        exit();
                }
            }
        }
    }
}
else{
    header("Location: login.php");
    exit();
}

?>