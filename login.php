<!DOCTYPE html>
<html>
<head>
    <title>LOGIN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="indexStyle.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">

    <?php
        include_once('header.php');
    ?>
    <br>
    <?php 
        include_once('nav.php');
    ?>
   <style>
        #content{
        height: 76vh;
        }
    </style>
</head>
<body>
    <div id="content">
        <form id="form" action="login1.php" method="post">
            <h2>LOGIN</h2>
            <?php if (isset($_GET['error'])){ ?>
                <p class="error"><?php echo $_GET['error']; ?></p>
            <?php } ?>
            <label>Email</label>
            <input type="text" name="email" placeholder="Email"><br>

            <label>Parola</label>
            <input type="password" name="password" placeholder="Parola"><br>

            <button type="submit">Login</button>
            <a href="loginAdmin.php" class="adminbutton">Admin</a>
        </form>
    </div>
    <?php
        include_once('footer.php');
    ?>
</body>
</html>