<!DOCTYPE html>
<head>
    <?php
        include_once('header.php');
    ?>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DetaliiPacient</title>
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <style>
        .cancelSbutton {
            position: absolute;
            top: 150px;
            right: 30px;
        }
        #content{
        height: 82vh;
        }
    </style>
</head>
<body>
    <br>
    <div id="content">
    <div class="container">
        <h2>Detalii Pacient</h2>
        <br>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nume</th>
                    <th>Prenume</th>
                    <th>CNP</th>
                    <th>Data Nasterii</th>
                    <th>Oras</th>
                    <th>Nume Mama</th>
                    <th>Nume Tata</th>
                    <th>Diagnostic</th>
                    <th>Istoric Familiar</th>
                    <th>Doctorul</th>

                </tr>
            </thead>
            <tbody>
            <?php include "db_conn.php";
            if(isset($_GET["id_pac"])){
                $id_pac = $_GET["id_pac"];
                $sql = "SELECT * from pacienti WHERE id_pac=$id_pac";
                $result = mysqli_query($conn, $sql);

                while($row = $result->fetch_assoc()){
                    echo"
                    <tr>
                    <td>$row[id_pac]</td>
                    <td>$row[nume]</td>
                    <td>$row[prenume]</td>
                    <td>$row[CNP]</td>
                    <td>$row[dataN]</td>
                    <td>$row[oras]</td>
                    <td>$row[mama]</td>
                    <td>$row[tata]</td>
                    <td>$row[diagnostic]</td>
                    <td>$row[istoric_fam]</td>
                    <td>$row[id_doc]</td>
                    <td><a class='addbutton' href='pd.php?id_pac=$row[id_pac]'>PDF</a></td>
                </tr>
                    ";
                }
                
                echo "<a class='cancelSbutton' href='vizualizare.php?id_pac=$id_pac'>Inapoi</a>";
            }
            ?>
            </tbody>
        </table>
    </div>
    </div>
    <?php
        include_once('footer.php');
    ?>
</body>
</html>