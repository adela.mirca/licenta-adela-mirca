<?php                
	include "db_conn.php";
	include_once('tcpdf/tcpdf.php');

	$id_pac=$_GET['id_pac'];

	$query = "SELECT id_pac, nume, prenume, CNP, dataN, oras, mama, tata, diagnostic, istoric_fam, id_doc
	FROM pacienti WHERE id_pac='$id_pac' ";             
	$result = mysqli_query($conn,$query);   
	$count = mysqli_num_rows($result);  
	if($count>0) 
	{
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

		$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);  
		$pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
		$pdf->SetDefaultMonospacedFont('times');  
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
		$pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
		$pdf->setPrintHeader(false);  
		$pdf->setPrintFooter(false);  
		$pdf->SetAutoPageBreak(false);  
		$pdf->SetFont('times', '', 12);  
		$pdf->AddPage(); //default A4

		$content .= '
		<style type="text/css">
		body{
		font-size:12px;
		line-height:24px;
		font-family: Cambria, Cochin, Georgia, Times, "Times New Roman", serif;
		background-color:#8ad7d1;
		}
		</style>    
		<table cellpadding="0" cellspacing="0" style="background-color:#d3d3d3; border:3px solid #8ad7d1;width:100%;height:100%;">
		<table style="width:100%;height:100%;" >
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2" align="center"><b>MENTAL HEALTH CLINIC</b></td></tr>
		<br>
		<tr><td colspan="2" align="center" style="font-size=5px;"><b>FISA PACIENT</b></td></tr>
		<br><br>
		<tr><td colspan="2" align="left" style="padding-left: 100px;"><b>ID: </b>'.$row['id_pac'].' </td></tr>
		<tr><td colspan="2" align="left" style="padding-left: 20px;"><b>Pacient:  </b>'.$row['nume'].' '.$row['prenume'].'</td></tr>
		<tr><td colspan="2" align="left" style="padding-left: 20px;"><b>CNP: </b> '.$row['CNP'].'</td></tr>
		<tr><td colspan="2" align="left" style="padding-left: 20px;"><b>Data Nasterii:  </b>'.$row['dataN'].'</td></tr>
		<tr><td colspan="2" align="left" style="padding-left: 20px;"><b>Oras:  </b>'.$row['oras'].'</td></tr>
		<tr><td colspan="2" align="left" style="padding-left: 20px;"><b>Nume mama:  </b>'.$row['mama'].' </td></tr>
		<tr><td colspan="2" align="left" style="padding-left: 20px;"><b>Nume tata:  </b>'.$row['tata'].' </td></tr>
		<tr><td colspan="2" align="left" style="padding-left: 20px;"><b>Istoric Familiar:  </b>'.$row['istoric_fam'].' </td></tr>
		<tr><td colspan="2" align="left" style="padding-left: 20px;"><b>ID doc:  </b>'.$row['id_doc'].' </td></tr>
		</table>
	</table>'; 
	$pdf->writeHTML($content);

	$file_location = "detaliiPac.php?id_pac=$id_pac"; 

	$datetime=date('dmY');
	$file_name = "Fisa_".$datetime.".pdf";
	ob_end_clean();

	$pdf->Output($file_name, 'D'); 
		
	}
	else
	{
		echo 'Record not found for PDF.';
	}
?>