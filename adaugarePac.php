<?php
    include "db_conn.php";

    $nume= "";
    $prenume= "";
    $CNP= "";
    $dataNasterii= "";
    $oras= "";
    $mama= "";
    $tata= "";
    $diagnostic= "";
    $simptome= "";
    $istoric_fam= "";
    $programare= "";
    $id_doc= "";
    $extra= "";
    $email= "";
    $password= "";

    $errorMessage= "";
    $successMessage= "";

    function valid_CNP ($input) // functie de validare
    {
            if (strlen($input) !== 13) {
                return false;
            }
            for ($i = 0; $i <=12; $i++) // imparte fiecare cifra a cnp-ului intr-un vector
            {
                $cnp[] = intval($input[$i]);
            }
            
            $suma = $cnp[0] * 2 + $cnp[1] * 7 + $cnp[2] * 9 + $cnp[3] * 1 + $cnp[4] * 4 + $cnp[5] * 6 + $cnp[6] * 3 + $cnp[7] * 5 + $cnp[8] * 8 + $cnp[9] * 2 + $cnp[10] * 7 + $cnp[11] * 9; //caluleaza o suma (face parte din algoritm)
            
            $rest = $suma % 11; // scoate restul din suma
            
            if (($rest < 10 && $rest == $cnp[12]) || ($rest == 10 && $cnp[12]==1)) // valideaza
                $validare = true;
            else 
                $validare = false;
            return $validare;
    }

    if( $_SERVER['REQUEST_METHOD'] == 'POST'){
        $nume= $_POST["nume"];
        $prenume= $_POST["prenume"];
        $CNP= $_POST["CNP"];
        $dataNasterii= $_POST["dataN"];
        $oras= $_POST["oras"];
        $mama= $_POST["mama"];
        $tata= $_POST["tata"];
        $diagnostic= $_POST["diagnostic"];
        $simptome= $_POST["simptome"];
        $istoric_fam= $_POST["istoric_fam"];
        $programare= $_POST["programare"];
        $id_doc= $_POST["id_doc"];
        $email= $_POST["email"];
        $password= $_POST["parola"];

        do{
            if(empty($nume) || empty($prenume) || empty($CNP) || empty($dataNasterii) || empty($oras) || empty($mama) || 
                empty($tata) || empty($diagnostic)||  
                empty($id_doc) || empty($email) || empty($password)){

                $errorMessage = "Completati toate campurile";
                break;
            }

            else if (valid_CNP($CNP) == false)
            {
                $errorMessage = "CNP incorect!";
            }

            //adaugare

            else{

                $sql = "INSERT INTO pacienti (nume, prenume, CNP, dataN, oras, mama, tata, diagnostic, 
                istoric_fam, id_doc) VALUES ('$nume', '$prenume', '$CNP', '$dataNasterii', '$oras', 
                '$mama', '$tata', '$diagnostic', '$istoric_fam', '$id_doc')";
                $result = mysqli_query($conn, $sql);
        
                if(!$result){
                    $errorMessage = "Invalid query";
                    break;
                }
        
                $nume= "";
                $prenume= "";
                $CNP= $_POST["CNP"];
                $dataNasterii= "";
                $oras= "";
                $mama= "";
                $tata= "";
                $diagnostic= "";
                $simptome= "";
                $istoric_fam= "";
                $programare= "";
                $id_doc= "";
                $extra= ""; 
                $email= $_POST["email"];
                $password= $_POST["parola"];
                $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

                $sql1 = "INSERT INTO logarePac (CNP, email, parola) VALUES('$CNP', '$email', '$hashedPassword')";
                $result1 = mysqli_query($conn, $sql1);
        
                if(!$result1){
                    $errorMessage = "Invalid query";
                    break;
                }
        
                $CNP= $_POST["CNP"];
                $email= "";
                $password= "";
            $successMessage = "Pacient adaugat";
            header("location: listaPacienti.php");
            exit;
        }
        }while(false);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>AdaugarePacienti</title>
        <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"></script>

        <?php
            include_once('header.php');
        ?>
        <br><br>
    </head>
    <body>
        <div class="container">
            <?php
            if(!empty($errorMessage)){
                echo "
                <div class='alert alert-warning alert-dismissible fade show' role='alert'>
                    <strong>$errorMessage</strong>
                    <button type='button' class='btn-close' data-bs-dismiss='alert' aria¡label='Close'></button>
                </div>
                ";
            }
            ?>
            <form id="addform" method="post">
            <h2>Pacient Nou</h2>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Nume</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="nume" value="<?php echo $nume; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Prenume</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="prenume" value="<?php echo $prenume; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">CNP</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="CNP" value="<?php echo $CNP; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Data Nasterii</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control" name="dataN" placeholder="Data Nasterii" value="<?php echo $dataNasterii; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Oras</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="oras" value="<?php echo $oras; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Nume Mama</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="mama" value="<?php echo $mama; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Nume Tata</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="tata" value="<?php echo $tata; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Diagnostic</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="diagnostic" value="<?php echo $diagnostic; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Istoric Familie</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="istoric_fam" value="<?php echo $istoric_fam; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">ID Doctor</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="id_doc" value="<?php echo $id_doc; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" name="email" value="<?php echo $email; ?>"><br>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-3 col-form-label">Parola</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="parola" value="<?php echo $password; ?>"><br>
                    </div>
                </div>
                <?php
                if(!empty($successMessage)){
                    echo "
                    <div class='row mb-3'>
                        <div clas='offset-sm-3 col-sm-6>
                            <div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <strong>$successMessage</strong>
                            <button type='button' class='btn-close' data-bs-dismiss='alert' aria¡label='Close'></button>
                            </div>
                        </div>
                    </div>
                ";
                }
                ?>
                <div class="row mb-3">
                    <div class="offset-sm-3 col-sm-3 d-grid">
                        <button type="submit" class="addbutton">Adaugati</button>
                    </div>
                    <div class="col-sm-3 d-grid">
                        <a class="cancelbutton" href="listaPacienti.php" role="button">Anulare</button></a>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>