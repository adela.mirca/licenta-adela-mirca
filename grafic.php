<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">
    <title>Grafic</title>
    <style>
        .cancelSbutton {
            position: absolute;
            top: 150px;
            right: 30px;
        }
        #content{
            height: 82vh !important;
        }
    </style>
    <?php
        include_once('header.php');
        echo"<br>";
    ?>
</head>
<body>
<div id="content">
    <?php
        include "db_conn.php";
        if(isset($_GET["id_pac"])){
            $id_pac = $_GET["id_pac"];

        echo"<a class='cancelSbutton' href='simptome.php?id_pac=$id_pac'>Inapoi</a>";

        $sql = "SELECT MONTH(data) AS monthname, COUNT(*) AS row_count FROM simptome WHERE id_pac = $id_pac GROUP BY monthname";
        $result = mysqli_query($conn, $sql);

        $monthname = [];
        $rowCount = [];

        while ($data = mysqli_fetch_assoc($result)) {
            $monthname[] = $data['monthname'];
            $rowCount[] = $data['row_count'];
        }
        }
    ?>

    <div style="width: 500px;">
        <canvas id="myChart"></canvas>
    </div>

    <script>
        const labels = <?php echo json_encode($monthname) ?>;
        const data = {
            labels: labels,
            datasets: [{
                label: 'Simptome',
                data: <?php echo json_encode($rowCount) ?>,
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }]
        };

        const config = {
            type: 'line',
            data: data,
            options: {
                scales: {
                    y: {
                beginAtZero: true,
                title: {
                    display: true,
                    text: 'nr simptome'
                }
            },
            x: {
                title: {
                    display: true,
                    text: 'luna'
                }
            }
                }
            },
        };

        var myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    </script>
</div>
    <?php
        include_once('footer.php');
    ?>
</body>
</html>
