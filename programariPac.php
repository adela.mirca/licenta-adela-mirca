<!DOCTYPE html>
<html>
    <head>
        <?php
            include_once('header.php');
            echo"<br>";
        ?>  
        <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">  
        <style>
            body{
                font-family: "Times New Roman", Times, serif;
            }
            .cancelSbutton {
                position: absolute;
                top: 150px;
                right: 30px;
            }
            #content{
                height: 82vh !important;
            }
        </style>
    </head>
    <body>
       

<div id="content">
    <div class="container">
        
            <?php 
                if(isset($_GET["id_pac"])){
                    $id_pac = $_GET["id_pac"];

                echo "<h2>Programarile Dvs:</h2>
                <br>
                <table class='table'>
                    <thead>
                        <tr>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>";
                include "db_conn.php";
                $sql = "SELECT * from programari  WHERE id_pac=$id_pac ORDER BY data ASC";
                $result = mysqli_query($conn, $sql);

                $row = $result->fetch_assoc();
                
                do{
                    echo"
                    <tr>
                    <td>$row[data]</td>
                    </tr>
                    ";
                    
                }while($row = $result->fetch_assoc());


                $sql1 = "SELECT * from pacienti  WHERE id_pac=$id_pac";
                $result1 = mysqli_query($conn, $sql1);
                $row1 = $result1->fetch_assoc();

                echo"<a class='cancelSbutton' href='MainPacient.php?CNP=$row1[CNP]'>Inapoi</a>";
                echo"<a class='addbutton' href='cerereProg.php?id_pac=$row1[id_pac]'>Cerere</a>";
            }
            ?>
            </tbody>
        </table>
    </div>
    </div>
        <?php
            include_once('footer.php');
     ?>  
    </body>

</html>