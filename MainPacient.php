<!DOCTYPE html>
<head>
    <?php
        include_once('header.php');
    ?>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MainPacient</title>
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <style>
        .cancelSbutton {
            position: absolute;
            top: 150px;
            right: 30px;
        }
        #content{
            height: 82vh !important;
        }
    </style>
</head>
<body>
    <br>
    <div id="content">
    <div class="container">
    <?php include "db_conn.php";
            if(isset($_GET["CNP"])){
                $CNP = $_GET["CNP"];

                $sql1 = "SELECT * from pacienti WHERE CNP=$CNP";
                $result1 = mysqli_query($conn, $sql1);
                

                while($row1 = $result1->fetch_assoc()){
                    $setReminder = $row1['reminder'];
                    if(!empty($setReminder))
                    {
                        echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                Daily Reminder: $setReminder
                                <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
                             </div>";
                    }
                }

echo"
        <h2>Detaliile Dvs.</h2>
        <table class='table'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nume</th>
                    <th>Prenume</th>
                    <th>CNP</th>
                    <th>Data Nasterii</th>
                    <th>Oras</th>
                    <th>Nume Mama</th>
                    <th>Nume Tata</th>
                    <th>Diagnostic</th>
                    <th>Istoric Familiar</th>
                    <th>Doctorul</th>

                </tr>
            </thead>
            <tbody>";
            

                
                
                
                //echo  "$CNP";

                $sql = "SELECT * from pacienti WHERE CNP=$CNP";
                $result = mysqli_query($conn, $sql);

                while($row = $result->fetch_assoc()){
                    echo"
                    <tr>
                    <td>$row[id_pac]</td>
                    <td>$row[nume]</td>
                    <td>$row[prenume]</td>
                    <td>$row[CNP]</td>
                    <td>$row[dataN]</td>
                    <td>$row[oras]</td>
                    <td>$row[mama]</td>
                    <td>$row[tata]</td>
                    <td>$row[diagnostic]</td>
                    <td>$row[istoric_fam]</td>
                    <td>$row[id_doc]</td>
                </tr>
                    ";
                    echo "<a class='cancelSbutton' href='index.php'>Log Out</a>";
                    echo "<a class='addbutton' href='programariPac.php?id_pac=$row[id_pac]'>Programari</a>";
                    echo "<a class='addbutton' href='simptomePac.php?id_pac=$row[id_pac]'>Simptome</a>";
                    echo"<br><br>";
                }
                
            }
            ?>
            </tbody>
        </table>
    </div>
    </div>
    <?php
    include_once('footer.php');
    ?>
</body>
</html>