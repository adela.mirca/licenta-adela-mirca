<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AdaugarePacienti</title>
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"></script>

    <style>
        #content{
        height: 82vh !important;
        }
    </style>
    <?php
        include_once('header.php');
        echo"<br>";

        echo"
        </head>";
        include "db_conn.php";

        $errorMessage= "";
        $successMessage= "";
        if(isset($_GET["id_pac"])){
            $id_pac = $_GET["id_pac"];
            
        if( $_SERVER['REQUEST_METHOD'] == 'POST'){
            $id_pac=$_POST["id_pac"];
            $data=$_POST["data"];

            do{
                if(empty($id_pac) || empty($data)){

                    $errorMessage = "Completati toate campurile";
                    break;
                }

                //adaugare

                $sql = "INSERT INTO programari (id_pac, data) VALUES ('$id_pac', '$data')";
                $result = mysqli_query($conn, $sql);

                if(!$result){
                    $errorMessage = "Invalid query";
                    break;
                }


                $successMessage = "Programare adaugata";

                $sql1 = "SELECT * from programari WHERE id_pac=$id_pac";
                $result1 = mysqli_query($conn, $sql1);
                $row = $result1->fetch_assoc();

                header("location: programari.php?id_pac=$row[id_pac]");
                exit;
            }while(false);
        }
                echo"
        
        <body>
        <div id='content'>
            <div class='container'>";
                if(!empty($errorMessage)){
                    echo"
                    <div class='alert alert-warning alert-dismissible fade show' role='alert'>
                        <strong>$errorMessage</strong>
                        <button type='button' class='btn-close' data-bs-dismiss='alert' aria¡label='Close'></button>
                    </div>";
                }
            echo"
                <form method='post' id='addPform'>
                <h2>Programare Noua</h2>
                    <div class='row mb-3'>
                        <label class='col-sm-3 col-form-label'>ID Pacient</label>
                        <div class='col-sm-6'>
                            <input type='text' class='form-control' name='id_pac' value='$id_pac'><br>
                        </div>
                    </div>
                    <div class='row mb-3'>
                        <label class='col-sm-3 col-form-label'>Data Programarii</label>
                        <div class='col-sm-6'>
                            <input type='datetime-local' class='form-control' name='data' value='$data'><br>
                        </div>
                    </div>";
                    if(!empty($successMessage)){
                        echo"<div class='row mb-3'>
                            <div clas='offset-sm-3 col-sm-6>
                                <div class='alert alert-success alert-dismissible fade show' role='alert'>
                                <strong>$successMessage</strong>
                                <button type='button' class='btn-close' data-bs-dismiss='alert' aria¡label='Close'></button>
                                </div>
                            </div>
                        </div>";
                    }
                    echo"
                    <div class='row mb-3'>
                        <div class='offset-sm-3 col-sm-3 d-grid'>
                            <button type='submit' class='addbutton'>Adaugati</button>
                        </div>
                        <div class='col-sm-3 d-grid'>
                            <a class='cancelbutton' href='programari.php?id_pac=$id_pac' role='button'>Anulare</button></a>
                            
                        </div>
                    </div>
                
                </form>
            </div>
        </div>
        </body>
        </html>";
        include_once('footer.php');
        }
?>