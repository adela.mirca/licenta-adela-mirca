<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CerereProgramare</title>
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"></script>

    <style>
        #addPform{
            height: 550px;
        }
        #content{
            height: 82vh !important;
        }
    </style>
    <?php
        include_once('header.php');
        echo"<br>";

        include "db_conn.php";

        $errorMessage= "";
        $successMessage= "";
        if(isset($_GET["id_pac"])){
            $id_pac = $_GET["id_pac"];
            
        if( $_SERVER['REQUEST_METHOD'] == 'POST'){
            $id_pac=$_POST["id_pac"];
            $data=$_POST["data"];
            $ora=$_POST["ora"];
            $nume=$_POST["nume"];
            $mesaj=$_POST["mesaj"];
        }
                echo"
        </head>
        <body>
        <div id='content'>
            <div class='container'>";
        if(!empty($errorMessage)){
            echo"
            <div class='alert alert-warning alert-dismissible fade show' role='alert'>
                <strong>$errorMessage</strong>
                <button type='button' class='btn-close' data-bs-dismiss='alert' aria¡label='Close'></button>
            </div>";
        }
    echo"
        <form id='addPform' method='post' action='sendMail.php'>
        <h2>Programare Noua</h2>
            <div class='row mb-3'>
                <label class='col-sm-3 col-form-label'>ID Pacient</label>
                <div class='col-sm-6'>
                    <input type='text' class='form-control' name='id_pac' value='$id_pac' required><br>
                </div>
            </div>
            <div class='row mb-3'>
                <label class='col-sm-3 col-form-label'>Nume si Prenume</label>
                <div class='col-sm-6'>
                    <input type='text' class='form-control' name='nume' value='$nume' required><br>
                </div>
            </div>
            <div class='row mb-3'>
                <label class='col-sm-3 col-form-label'>Data Programarii</label>
                <div class='col-sm-6'>
                    <input type='date' class='form-control' name='data' value='$data' required><br>
                </div>
            </div>
            <div class='row mb-3'>
                <label class='col-sm-3 col-form-label'>Ora Dorita</label>
                <div class='col-sm-6'>
                    <input type='time' class='form-control' name='ora' value='$ora' required><br>
                </div>
            </div>
            <div class='row mb-3'>
                <label class='col-sm-3 col-form-label'>Mesaj (optional)</label>
                <div class='col-sm-6'>
                    <input type='text' class='form-control' name='mesaj' value='$mesaj'><br>
                </div>
            </div>
            ";
            if(!empty($successMessage)){
                echo"<div class='row mb-3'>
                    <div clas='offset-sm-3 col-sm-6>
                         <div class='alert alert-success alert-dismissible fade show' role='alert'>
                         <strong>$successMessage</strong>
                         <button type='button' class='btn-close' data-bs-dismiss='alert' aria¡label='Close'></button>
                         </div>
                    </div>
                </div>";
            }
            echo"
            <div class='row mb-3'>
                <div class='offset-sm-3 col-sm-3 d-grid'>
                <button type='submit' class='addbutton'>Trimitere</button>
                </div>
                <div class='col-sm-3 d-grid'>
                    <a class='cancelbutton' href='programariPac.php?id_pac=$id_pac' role='button'>Anulare</button></a>
                </div>
            </div>
        
        </form>
    </div>
</div>
</body>
</html>";

            include_once('footer.php');
         
        }
?>