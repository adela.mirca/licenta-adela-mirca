<!DOCTYPE html>
<head>
    <?php
        include_once('header.php');
    ?>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ListaPacienti</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css">
    <link href="indexStyle.css" rel="stylesheet" type="text/css"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        #content{
        height: 82vh;
        }
    </style>
</head>
<body>
    <br>
    <div id="content">
    <div class="container">
    <a class='outbutton' href='index.php'>Log Out</a>
        <h2>Lista Doctori</h2>
        <br>
        <a class='addbutton' href="adaugareDoc.php">Adaugare</a>
        
                    
        <br>
        <br>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nume</th>
                    <th>Prenume</th>
                    <th>CNP</th>
                    <th>Actiune</th>
                </tr>
            </thead>
            <tbody>
            <?php include "db_conn.php";
                $sql = "SELECT * from logareDoc";
                $result = mysqli_query($conn, $sql);

                while($row = $result->fetch_assoc()){
                    $CNP=$row['CNP'];
                    echo"
                    <tr>
                    <td>$row[id_doc]</td>
                    <td>$row[nume]</td>
                    <td>$row[prenume]</td>
                    <td>$CNP</td>
                    <td>
                        <button type='button' class='deletebutton' data-id='$CNP' onclick='confirmDelete(this);'>Stergere</button>
                    </td>
                </tr>
                    ";
                }
            ?>
            </tbody>
        </table>
        <br>
        

<form method="POST" action="" id="form-delete-user" style="display: none;">
    <input type="hidden" name="CNP">
</form>

<script>
    function confirmDelete(self) {
        var CNP = self.getAttribute("data-id");

        swal({
            title: 'Acest doctor va fi sters',
            text: 'Asigurativa ca pacientii sai sunt redistribuiti mai intai',
            icon: 'warning',
            buttons:
            {
                cancel: {
                    text: 'Anulare',
                    value: false,
                    visible: true,
                    className: 'custom-cancel-button',
                    closeModal: true,
                },
                confirm: {
                    text: 'Stergere',
                    value: true,
                    visible: true,
                    className: 'custom-confirm-button',
                    closeModal: true
                }
            },
            dangerMode: true,
        }).then((confirmed) => {
            if (confirmed) {
                document.getElementById("form-delete-user").action = "stergereDoc.php?CNP=" + CNP;
                document.getElementById("form-delete-user").CNP.value = CNP;
                document.getElementById("form-delete-user").submit();
            }
        });
    }
</script>
    </div>
    </div>
    <?php
        include_once('footer.php');
    ?>
</body>
</html>